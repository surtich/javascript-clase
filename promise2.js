function promiseInc(x) {
    return new Promise(function (resolve, reject) {
        if (typeof x !== "number") {
            reject("Error.");
        } else {
            setTimeout(function () {
                resolve(x + 1);
            }, 0);
        }
    });
}

var xs = [1, 2, 4, 3];
var p = Promise.resolve([]);

for (var i = 0; i < xs.length; i++) {
    var x = xs[i];

    (function (x) {
        p = p.then(ys => promiseInc(x).then(y => ([...ys, y])));
    })(x)

}

p.then(ys => console.log(ys));


Promise.all(xs.map(promiseInc)).then(console.log);

function promiseAdd(x, y) {
    return new Promise(function (resolve, reject) {
        if (typeof x !== "number" || typeof y !== "number") {
            reject("Error.");
        } else {
            setTimeout(function () {
                resolve(x + y);
            }, 0);
        }
    });
}

function asyncSum(xs) {
    var p = Promise.resolve(0);
    for (var i = 0; i < xs.length; i++) {
        var x = xs[i];
        
        (function (x) {
            p = p.then(sum => promiseAdd(x, sum));
        })(x)
        
    }

    return p;
}

function asyncSum2(xs) {
    return xs.reduce((psum, x) => psum.then(sum => promiseAdd(sum, x)), Promise.resolve(0));
}

function asyncReduce(f, init, xs) {
    return xs.reduce((pacc, x) => pacc.then(acc => f(acc, x)), Promise.resolve(init));
}

var asyncSum3 = xs => asyncReduce(promiseAdd, 0, xs);


asyncSum(xs).then(console.log);
asyncSum2(xs).then(console.log);
asyncSum3(xs).then(console.log);

function promiseAll(ps) {
    return new Promise(function(resolve, reject) {
        var xs = [];
        var steps = 0;
        for (var i = 0; i < ps.length; i++) {
            var p = ps[i];
            (function(i) {
                p.then(v => {
                    xs[i] = v;
                    if (steps === ps.length) {
                        resolve(xs);
                    }
                }).catch(err => reject(err));
            }(i))
        }

        return xs;
    });
}

promiseAll(xs.map(promiseInc)).then(console.log);

promiseAll([1,"fddfd", 2, 3].map(promiseInc)).then(console.log).catch(console.log);
