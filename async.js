function add(x, y) {
    return x + y;
}

function product(x, y) {
    return x * y;
}

function inc(x) {
    return x + 1;
}


function asyncAdd(x, y, callback) {
    setTimeout(function () {
        if (typeof x !== "number" || typeof y !== "number") {
            callback("Error")
        } else {
            callback(null, x + y);
        }
    }, 0);
}

function asyncProduct(x, y, callback) {
    setTimeout(function () {
        callback(x * y);
    })
}

function asyncInc(x, callback) {
    setTimeout(function () {
        callback(x + 1);
    })
}


asyncInc(5, function (x) {
    asyncAdd("deeddd", 4, function (err, y) {
        if (err) {
            console.log(err)
        } else {
            asyncProduct(x, y, function (z) {
                asyncInc(z, function (a) {
                    console.log(a);
                })
            })
        }
    })
});