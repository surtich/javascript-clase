function add(x, y) {
    return x + y;
}

function product(x, y) {
    return x * y;
}

function inc(x) {
    return x + 1;
}

function asyncAdd(x, y, callback) {
    setTimeout(function () {
        callback(x + y);
    }, 0);
}

function asyncProduct() {
    setTimeout(function () {
        callback(x * y);
    }, 0);
}

function asyncInc() {
    setTimeout(function () {
        callback(x + 1);
    }, 0);
}

function promiseAdd(x, y) {
    return new Promise(function (resolve, reject) {
        if (typeof x !== "number" || typeof y !== "number") {
            reject("Error.");
        } else {
            setTimeout(function () {
                resolve(x + y);
            }, 0);
        }
    });
}

function promiseProduct(x, y) {
    return new Promise(function (resolve, reject) {
        if (typeof x !== "number" || typeof y !== "number") {
            reject("Error.");
        } else {
            setTimeout(function () {
                resolve(x * y);
            }, 0);
        }
    });
}

function promiseInc(x) {
    return new Promise(function (resolve, reject) {
        if (typeof x !== "number") {
            reject("Error.");
        } else {
            setTimeout(function () {
                resolve(x + 1);
            }, 0);
        }
    });
}


Promise.all([promiseInc(5), promiseAdd(3, 4)])
    .then(([x, y]) => promiseProduct(x, y))
    .then(promiseInc)
    .then(console.log)
    .catch(console.log);


