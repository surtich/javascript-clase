function doubleArray(numbers) {
    var doubleNumbers = [];
    for (var i = 0; i < numbers.length; i++) {
        doubleNumbers.push(numbers[i] * 2);
    }

    return doubleNumbers;
}

function incArray(numbers) {
    var incNumbers = [];
    for (var i = 0; i < numbers.length; i++) {
        incNumbers.push(numbers[i] + 1);
    }

    return incNumbers;
}



var x = [1, 4, 6];
var y = doubleArray(x);
var z = incArray(x);

initials(["Pepe", "Luis", "Ana"]); // ["P", "L", "A"]
sizes(["Pepe", "Luis", "Ana"]); // [4, 4, 3]





console.log("x=", x);
console.log("y=", y);
console.log("z=", z);