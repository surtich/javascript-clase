function promiseAdd(x, y) {
    return new Promise(function (resolve, reject) {
        if (typeof x !== "number" || typeof y !== "number") {
            reject("Error.");
        } else {
            setTimeout(function () {
                resolve(x + y);
            }, 0);
        }
    });
}

function promiseProduct(x, y) {
    return new Promise(function (resolve, reject) {
        if (typeof x !== "number" || typeof y !== "number") {
            reject("Error.");
        } else {
            setTimeout(function () {
                resolve(x * y);
            }, 0);
        }
    });
}

function promiseInc(x) {
    return new Promise(function (resolve, reject) {
        if (typeof x !== "number") {
            reject("Error.");
        } else {
            setTimeout(function () {
                resolve(x + 1);
            }, 0);
        }
    });
}
// (5 + 1) * (3 + 4) + 1  = 43



(async function () {
    var x = await promiseInc(5);
    var y = await promiseAdd(3, 4);
    var z = await promiseProduct(x, y);
    console.log(await promiseInc(z));

    console.log(await promiseInc(await promiseProduct(await promiseInc(5), await promiseAdd(3, 4))));

    var xs = [1, "ddd", 4, 3];

    var s = 0;

    for (var i = 0; i < xs.length; i++) {
        try {
            s = await promiseAdd(s, xs[i]);
        } catch (e) {
            console.log(e);
        }
    }
    console.log(s);


})();

